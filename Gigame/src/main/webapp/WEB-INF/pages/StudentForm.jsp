<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>New Student</title>
</head>
<body>
    <div align="center">
        <h1>New Student</h1>
        <form:form action="saveStudent" method="post" modelAttribute="student">
        <table>
            <form:hidden path="regNo"/>
            <tr>
                <td>Name:</td>
                <td><form:input path="name" /></td>
            </tr>
            <tr>
                <td>Marks1:</td>
                <td><form:input path="marks1" /></td>
            </tr>
            <tr>
                <td>Marks2:</td>
                <td><form:input path="marks2" /></td>
            </tr>
            <tr>
                <td>Marks3:</td>
                <td><form:input path="marks3" /></td>
            </tr>
            <tr>
                <td>Total:</td>
                <td><form:input path="total" /></td>
            </tr>
            <tr>
                <td>Average:</td>
                <td><form:input path="avg" /></td>
            </tr>
            <tr>
                <td colspan="2" align="center"><input type="submit" value="Save"></td>
            </tr>
        </table>
        </form:form>
    </div>
</body>
</html>