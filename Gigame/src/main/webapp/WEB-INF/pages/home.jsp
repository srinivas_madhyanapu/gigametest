<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Student Records</title>
</head>
<body>
	<div align="center">
		<h1>Students List</h1>
		
		<table border="1">

			<th>Registration Id</th>
			<th>Name</th>
			<th>Marks1</th>
			<th>Marks2</th>
			<th>Marks3</th>
			<th>Total</th>
			<th>Average</th>
			<th>Actions</th>
			<c:forEach var="student" items="${listStudent}">
				<tr align="center">
					<td>${student.regNo}</td>
					<td>${student.name}</td>
					<td>${student.marks1}</td>
					<td>${student.marks2}</td>
					<td>${student.marks3}</td>
					<td>${student.total}</td>
					<td>${student.avg}</td>
					<td><a href="editStudent?regNo=${student.regNo}">Edit</a>
						&nbsp;&nbsp;&nbsp;&nbsp; 
						<a href="deleteStudent?regNo=${student.regNo}">Delete</a></td>

				</tr>
			</c:forEach>
		</table>
		<h4>
			New Student Register <a href="newStudent">here</a>
		</h4>
	</div>
</body>
</html>