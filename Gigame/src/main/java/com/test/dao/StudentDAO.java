package com.test.dao;

import java.util.List;

import com.test.entity.Student;

public interface StudentDAO {

	public void addStudent(Student student);

	public List<Student> getAllStudents();

	public void deleteStudent(int regNo);

	public Student updateStudent(Student student);

	public Student getStudent(int regNo);
}
