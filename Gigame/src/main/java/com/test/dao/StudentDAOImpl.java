package com.test.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.test.entity.Student;

@Repository
public class StudentDAOImpl implements StudentDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void addStudent(Student student) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().saveOrUpdate(student);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Student> getAllStudents() {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createQuery("from Student").list();
	}

	@Override
	public void deleteStudent(int regNo) {
		// TODO Auto-generated method stub
		Student student= (Student) sessionFactory.getCurrentSession().load(Student.class, regNo);
		if (null != student) {
			this.sessionFactory.getCurrentSession().delete(student);
		}
	}

	@Override
	public Student updateStudent(Student student) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().update(student);
		return student;
	}

	@Override
	public Student getStudent(int regNo) {
		// TODO Auto-generated method stub
		return (Student) sessionFactory.getCurrentSession().get(Student.class, regNo);

	}

}
