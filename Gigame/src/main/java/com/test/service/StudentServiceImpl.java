package com.test.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.test.dao.StudentDAO;
import com.test.entity.Student;

@Service
@Transactional
public class StudentServiceImpl implements StudentService {

	@Autowired
	private StudentDAO studentDAO;
	
	@Override
	public void addStudent(Student student) {
		// TODO Auto-generated method stub
			studentDAO.addStudent(student);
	}

	@Override
	public List<Student> getAllStudents() {
		// TODO Auto-generated method stub
		return studentDAO.getAllStudents();
	}

	@Override
	public void deleteStudent(int regNo) {
		// TODO Auto-generated method stub
		studentDAO.deleteStudent(regNo);
	}

	@Override
	public Student getStudent(int regNo) {
		// TODO Auto-generated method stub
		return studentDAO.getStudent(regNo);
	}

	@Override
	public Student updateStudent(Student student) {
		// TODO Auto-generated method stub
		return studentDAO.updateStudent(student);
	}
	
	public void setStudentDAO(StudentDAO studentDAO) {
		this.studentDAO = studentDAO;
	}

}
