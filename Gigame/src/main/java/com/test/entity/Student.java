package com.test.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="student_details")
public class Student implements Serializable{

	@Id
	@Column(name = "regNo")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int regNo;
	@Column(name = "name")
	private String name;
	@Column(name = "marks1")
	private double marks1;
	@Column(name = "marks2")
	private double marks2;
	@Column(name = "marks3")
	private double marks3;
	@Column(name = "total")
	private double total;
	@Column(name = "avg")
	private double avg;
	public int getRegNo() {
		return regNo;
	}
	public void setRegNo(int regNo) {
		this.regNo = regNo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getMarks1() {
		return marks1;
	}
	public void setMarks1(double marks1) {
		this.marks1 = marks1;
	}
	public double getMarks2() {
		return marks2;
	}
	public void setMarks2(double marks2) {
		this.marks2 = marks2;
	}
	public double getMarks3() {
		return marks3;
	}
	public void setMarks3(double marks3) {
		this.marks3 = marks3;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public double getAvg() {
		return avg;
	}
	public void setAvg(double avg) {
		this.avg = avg;
	}
	
	@Override
	public String toString() {
		return "Student [regNo=" + regNo + ", name=" + name + ", marks1=" + marks1 + ", marks2=" + marks2 + ", marks3="
				+ marks3 + ", total=" + total + ", avg=" + avg + "]";
	}
	
	
	
}
